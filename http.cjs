const http = require('http')
const fs = require('fs')
const uuid = require('crypto')
const port = require('./config.js')

const creatingServer = http.createServer((req, res) => {
    if (req.url === '/html') {
        res.setHeader('Content-type', 'text/html')
        fs.readFile('./file.html', ((error, data) => {
            if (error) {
                console.log(error)
            }
            else {
                res.end(data)
            }
        }))
    }
    else if (req.url === '/json') {
        res.setHeader('Content-type', 'application/json')

        fs.readFile('./file.json', 'utf-8', ((error, data) => {
            if (error) {
                console.log(error)
            }
            else {
                res.end(data)
            }
        }))
    }
    else if (req.url === ('/uuid')) {
        res.setHeader('Content-type', 'application/json')

        res.end(JSON.stringify({
            'uuid': uuid.randomUUID()
        }))
    }
    else if (req.url.startsWith('/status')) {
        const statusCodeArray = req.url.split('/')

        const statusCode = req.url.split('/')[2]
        if (statusCode in http.STATUS_CODES && statusCodeArray.length === 3)
        {
            res.statusCode = statusCode
            res.end(JSON.stringify({ "Status code": statusCode }))
        }
        else {
            res.statusCode = 400
            res.end(JSON.stringify({ "Status code": "Invalid" }))
        }
    }
    else if (req.url.startsWith('/delay')) {
        const secondsDelayArr = req.url.split('/')
        const secondsDelay = +req.url.split('/')[2]
        res.setHeader('Content-type', 'application/json')
        if (secondsDelay < 0) {
            res.statusCode = 400
            res.end(JSON.stringify({ "Message": "Can not go back in time" }))

        }
        else if (secondsDelay >= 0 && secondsDelayArr.length === 3) {
            setTimeout(() => {
                res.statusCode = 200
                res.end(`{"Responce after": "${secondsDelay} Seconds"}`)
            }, secondsDelay * 1000);

        }
        else {
            res.statusCode = 400
            res.end(JSON.stringify({ "Message": "Please Enter a valid number" }))
        }
    }
    else {
        res.setHeader('Content-type', 'text/html')
        res.statusCode = 400
        res.end(`<h1>Homepage<h1>`)
    }
})

creatingServer.listen(port, () => {
    console.log(`${port} Server running`);
})